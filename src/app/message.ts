export class Message {

  public firstLetterOfName?: string;

  constructor(public sender?: string,
              public message?: string,
              public type?: string) {
    this.firstLetterOfName = Message.getFirstLetterOfSender(sender);
  }

  private static getFirstLetterOfSender(sender: string): string {
    if (sender) {
      return sender.charAt(0);
    }
    return null;
  }
}
