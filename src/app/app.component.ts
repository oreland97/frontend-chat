import {Component, ElementRef, ViewChild} from '@angular/core';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import {Message} from "./message";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private serverUrl = 'http://localhost:8080/ws';
  title = 'WebSockets chat';

  @ViewChild('username') usernamePage: ElementRef;
  @ViewChild('chat') chatPage: ElementRef;
  @ViewChild('messageInput') messageInput: ElementRef;
  @ViewChild('messageArea') messageArea: ElementRef;

  private readonly stompClient: Stomp;
  private readonly socket: SockJS;
  private username: string;
  protected hideUsernamePage: boolean;
  protected hideChat: boolean;
  protected userJoined: boolean;
  protected userLeft: boolean;
  private static messages: Message[] = [];

  private colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
  ];

  constructor() {
    this.hideUsernamePage = true;
    this.hideChat = false;
    this.socket = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(this.socket);
    this.userJoined = false;
    this.userLeft = false;
  }

  public get messages(): Message[] {
    return AppComponent.messages;
  }

  submitFormWithName(event, name): void {
    this.username = name;

    if (this.username) {
      this.hideUsernamePage = false;
      this.hideChat = true;

      this.stompClient.connect({});
      this.stompClient.send('/app/chat.addUser', {},
        JSON.stringify(
          {
            sender: this.username,
            type: 'JOIN'
          }));
      this.stompClient.subscribe('/topic/public', this.onMessageReceived);
    }
    event.preventDefault();
  }

  sendMessage(event, message): void {
    let messageContent = message.trim();
    if (messageContent && this.stompClient) {
      let chatMessage = {
        sender: this.username,
        content: messageContent,
        type: 'CHAT'
      };
      this.stompClient.send('/app/chat.sendMessage', {},
        JSON.stringify(chatMessage));
      this.messageInput.nativeElement.value = '';
    }
    event.preventDefault();
  }

  onMessageReceived(payload): void {
    let message = JSON.parse(payload.body);

    if (message.type === 'JOIN') {
      AppComponent.messages.push(new Message(message.sender, null, message.type));
    }
    else if (message.type === 'LEAVE') {
      AppComponent.messages.push(new Message(message.sender, null, message.type));
    }
    else {
      AppComponent.messages.push(new Message(message.sender, message.content, message.type));
    }
  }

  getAvatarColor(messageSender): string {
    let hash = 0;
    for (let i = 0; i < messageSender.length; i++) {
      hash = 31 * hash + messageSender.charCodeAt(i);
    }
    let index = Math.abs(hash % this.colors.length);
    return this.colors[index];
  }
}
